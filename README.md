Launch script and systemd service for a gstreamer-based RTSP video stream in a Raspberry Pi device

---

# Dependencies
***Built and tested on Raspbian Buster lite***

## GStreamer and RTSP server
```
sudo apt-get update && sudo apt-get dist-upgrade -y
sudo apt-get install libraspberrypi-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-tools gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly libgstrtspserver-1.0-dev
```

## GStreamer adapter for raspberry pi cam
```
git clone https://github.com/thaytan/gst-rpicamsrc.git && cd gst-rpicamsrc
./autogen.sh --prefix=/usr --libdir=/usr/lib/arm-linux-gnueabihf
make
sudo make install
```

---

# Build
```
git clone https://gitlab.com/JacoboFanjul/camera-rtsp-server.git && cd camera-rtsp-server
gcc test-launch.c -o test-launch $(pkg-config --cflags --libs gstreamer-1.0 gstreamer-rtsp-server-1.0)
```

# Configure as system service
In order for the camera stream to be available automatically at device boot:
- Edit the **ExecStart** and **WorkingDirectory** fields in the rtsp-stream.service file.
- Run the following commands:
```
sudo cp rtsp-stream.service /etc/systemd/system/
sudo systemctl start rtsp-stream.service
sudo systemctl enable rtsp-stream.service
```

# Uninstall
The camera streamer can be uninstalled as a system service by simply running the following lines:
```
sudo systemctl stop rtsp-stream.service
sudo systemctl disable rtsp-stream.service
```

